"""
Jenna Baruch
5/9/19
Alphabet Soup
"""

import sys

"create 2d array for different search direction possibilites"
DIRECTIONS = [
        [-1, -1],
        [-1, 0],
        [-1, 1],
        [0, -1],
        [0, 1],
        [1, -1],
        [1, 0],
        [1, 1]
        ]

"constants for search"
ROW = 0
COL = 1

def get(grid, i, j, dir, length, letter):
    """Verifies that the first letter matches and that length <= bounds
    Extracts and returns string if above conditions are met
    """
    if (letter!=grid[i][j]):
        return None
    if ( 0 <= i + (length - 1) * dir[ROW] < len(grid) and
         0 <= j + (length - 1) * dir[COL] < len(grid[i]) ):
        return ''.join(
            grid[i + n * dir[ROW]][j + n * dir[COL]] for n in range(length)
        )
    return None


def search (grid, wordToSearch):
    """Iterates through grid and directions
    Calls get to extract strings in grid for each index and direction
    Compares return from get method to search word
    Returns starting coordinates and direction of matching string
    """
    word_len = len(wordToSearch)
    for i in range (len(grid)):
        for j in range (len(grid[0])):
            for dir in DIRECTIONS:
                "if word matches extracted string"
                if wordToSearch == get(grid, i, j, dir, word_len, wordToSearch[0]):
                    return i, j, dir
    return None

"open file to be read"
myFile = open(sys.argv[1], "r")
contents = myFile.read()
"split contents by line"
contents = contents.split("\n")

"get dimensions"
size = contents[0].split("x")
rowLength = int(size[0])
columnLength = int(size[1])

"add wordsearch into 2d array grid"
l = 0
j = 0
grid = [ [ None for l in range(columnLength) ] for j in range(rowLength) ]

i = 0
"iterate through contents and add .txt word search into a 2d array grid"
for x in range (0,rowLength):
    for y in range (0, columnLength):
        grid[x][y] = (contents[x+1])[i]
        i = i + 2
    i = 0

numWords = (len(contents) - (rowLength+1) )
wordsArray = [None]* numWords

"find words to search for"
for x in range (0, numWords):
    "get rid of spaces if they exist within search words"
    wordsArray[x] = contents[x + rowLength+ 1].replace(" ", "")

"for each word that needs to be searched for"
for x in range ( 0, len(wordsArray)):
    "call search to find the word in the grid"
    match = search(grid, wordsArray[x])
    "set variable to find ending coordinates"
    n = len(wordsArray[x]) - 1
    if match == None:
        print("Didn't find {0} in the grid".format(wordsArray[x]))
    else:
        i, j, dir = match
        "print word and coordinates in correct display format"
        print(wordsArray[x] + " " + str(i) + ":" + str(j) + " " + str(i+(n*dir[ROW])) +":" + str(j+(n*dir[COL])))